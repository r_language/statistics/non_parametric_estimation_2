# Non_parametric_estimation_survival_curve_2



In the R file provided there is a code that shows how to do an estimation of a survival curve with a non parametric method. To do so, you will need the file smoking.csv that you can download. In the code we use multiple functions such as survfit, fitdistcens, pweibull, pgamma, dweibull, and muhaz.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021
